module swap_module(

	/* this is my input signal from other module - W.i.P */
	
	
	/* this is my hardware input signal */
	input	clk,
	input	reset,
	input	left_bt,
	input	right_bt,
	input	choose_bt,
	input game_bt,
	input	sw_done,
	input enable,
	
	/* this get from the word generator module */
	//input	[6:0]	seg_inChar0,
	//input	[6:0]	seg_inChar1,
	//input	[6:0]	seg_inChar2,
	/* this goes to 7 segment module for display */	
	output	[6:0]	seg_outChar0,
	output	[6:0]	seg_outChar1,
	output	[6:0]	seg_outChar2,	
	/* and this shit goes to my cursor control module */
	output	[2:0]	cursor_indicator
	);

reg [6:0] 	swap_temp;

reg [6:0]	myOut_char[0:2]; //  1-D char array, this is not C or python, WTF is this ?  i hate this shit

reg [2:0]	myCursor_indicator;

reg [1:0]	index_swap0;

reg [1:0]	index_swap1;

reg [1:0]	swap_status;

reg game_flag;

reg swap_mode; 

always @(posedge clk) begin 

	if(reset == 0) begin
			myCursor_indicator = 3'b100;		
			index_swap0 = 0;			
			index_swap1 = 0;			
			swap_status = 2'b00;				
			game_flag = 0;
			swap_mode = 0;
			/* assign my input */
			myOut_char[0] = 7'b1111111;
			myOut_char[1] = 7'b1111111;
			myOut_char[2] = 7'b1111111;
		
	end
	
	else if(game_bt == 0 && game_flag == 0) begin //loading the scramble word, won't allow to load when on swapping mode
	
			myCursor_indicator = 3'b100;			
			index_swap0 = 0;			
			index_swap1 = 0;		
			swap_status = 2'b00;
			myOut_char[0] = 7'b0001000; // myOut_char = seg_inChar0;
			myOut_char[1] = 7'b1000110; // myOut_char = seg_inChar1; 
			myOut_char[2] = 7'b0001111; // myOut_char = seg_inChar2;
			
			game_flag = 1;
	end
	
	else if(enable == 1 && sw_done == 1 && swap_mode == 1) begin
			game_flag = 0;
			swap_mode = 0;
	end
	
	else begin
		
		if(enable == 1 && sw_done == 0 && game_flag == 1) begin
			/* move left */
			swap_mode = 1;
			if(left_bt == 0)	
				myCursor_indicator = myCursor_indicator << 1 | myCursor_indicator >> (3 - 1); // never heard of circular shift, google it.
			
			
			if(right_bt == 0) 
				myCursor_indicator = myCursor_indicator >> 1 | myCursor_indicator << (3 - 1); // never heard of circular shift, google it.
			
					
			if(choose_bt == 0) begin
			
				case(myCursor_indicator)		
					 3'b001:
						begin
						
							if(swap_status == 0)						
								index_swap0 = 0;					
							else
								index_swap1 = 0;
						end
						
					3'b010:
						begin
						
							if(swap_status == 0) // first charac										
								index_swap0 = 1;		
							else 
								index_swap1 = 1;
							
						end
					3'b100:
						begin
						
							if(swap_status == 0)  // first charac
								index_swap0 = 2;						
							else 							
								index_swap1 = 2;
								
						end
					default:
						begin
						
						index_swap0 = 0;
						
						index_swap1 = 0;
						
						end
						
				endcase
				
				swap_status = swap_status + 1;
				
				if(swap_status == 2'b10) begin
				
					swap_status = 0;
					
					swap_temp = myOut_char[index_swap0];
					
					
					myOut_char[index_swap0] = myOut_char[index_swap1];
					
				
					myOut_char[index_swap1] = swap_temp;	
					
				end
				
			
			end
			
			
		end
		
	end
				
	
end


/* assign my output */
assign seg_outChar0 = myOut_char[0];  
assign seg_outChar1 = myOut_char[1];  
assign seg_outChar2 = myOut_char[2];
assign cursor_indicator = myCursor_indicator;  

endmodule 


	