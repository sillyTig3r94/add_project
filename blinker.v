module blinker(

	input clk,
	input clk_1hz,
	input reset,
	input enable,
	input [2:0] position,
	
	input sw_done,
	input choose_bt,
	
	input	 [6:0] in_seg0,
	input	 [6:0] in_seg1,
	input  [6:0] in_seg2,
	
	output reg [6:0] out_seg0,
	output reg [6:0] out_seg1,
	output reg [6:0] out_seg2	
	);
	
reg status ;


always @(posedge clk_1hz, negedge reset, negedge choose_bt) begin

	if(reset == 0)
		begin
			status = 0;
			out_seg0 = in_seg0; 
			out_seg1 = in_seg1;
			out_seg2 = in_seg2;
		end
		
	else if(choose_bt == 0) 
		begin
			out_seg0 = in_seg0; 
			out_seg1 = in_seg1;
			out_seg2 = in_seg2;
		end
		
	else begin
		
			if(status == 0 && enable == 1) begin	//display when status = 0
			
				status = 1;
				
				if(sw_done == 0) begin
				
					case (position)	
					
					3'b001: out_seg0 = 7'b1111111;
						
					3'b010: out_seg1 = 7'b1111111;
										
					3'b100: out_seg2 = 7'b1111111;				
	 
					endcase
			
				end
				
			end
			
			else begin	// remove when status = 1, don't ask why ~_~
			
				status = 0;
				
				//and do nothing just display
				
				out_seg0 = in_seg0;
			
				out_seg1 = in_seg1;
				
				out_seg2 = in_seg2;
			
			end
				
		
		end

end

endmodule
	

		