module swap(

	/* this is my input signal from other module - W.i.P */
	
	
	/* this is my hardware input signal */
	input	clk,
	input	reset,
	input	bt_left,
	input	bt_right,
	input	bt_choose,
	input	sw_done,
	input enable,
	
	/* this get from the word generator module */
	//input	[6:0]	seg_inChar0,
	//input	[6:0]	seg_inChar1,
	//input	[6:0]	seg_inChar2,
	/* this goes to 7 segment module for display */	
	output	[6:0]	seg_outChar0,
	output	[6:0]	seg_outChar1,
	output	[6:0]	seg_outChar2,	
	/* and this shit goes to my cursor control module */
	output	[2:0]	cursor_indicator,
	/* this for score tracking result */
	output	game_flag, //debug will removed
	output	[6:0] seg_score
	);

	wire left_bt;
	wire right_bt;
	wire choose_bt;
	wire game_bt;
	wire confirm_bt;
	wire [3:0] score;
	
	wire clk_1hz;
	
	wire [6:0] blink_seg0;
	wire [6:0] blink_seg1;
	wire [6:0] blink_seg2;
	
	
button_shaper left_button(bt_left,clk,reset,left_bt);

button_shaper right_button(bt_right,clk,reset,right_bt);

button_shaper choose_button(bt_choose,clk,reset,choose_bt);

button_shaper game_button(bt_choose,clk,reset,game_bt); // game button used for generate a random word

button_shaper confirm_button(bt_left,clk,reset, confirm_bt);

clock_1hz clock_5hz(clk,reset,enable,clk_5hz);

decode_7seg score_display(score,seg_score);


swap_module swap_init(	
			clk,
			reset,
			left_bt,
			right_bt,
			choose_bt,
			game_bt,
			sw_done,
			enable,
			blink_seg0,
			blink_seg1,
			blink_seg2,
			cursor_indicator);
			
blinker idgaf(
	clk,
	clk_5hz,
	reset,
	enable,
	cursor_indicator,
	sw_done,
	choose_bt,
	blink_seg0,
	blink_seg1,
	blink_seg2,
	seg_outChar0,
	seg_outChar1,
	seg_outChar2);
	
	
score_tracking stfu(
	clk,		
	reset,
	enable,
	sw_done,
	game_bt,
	confirm_bt,
	blink_seg0,
	blink_seg1,
	blink_seg2,
	game_flag, //debug will removed
	score);
	

	


endmodule 



	