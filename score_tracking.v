module score_tracking(
	
	//set up word from 
//	input [6:0] word_check0,
//	input [6:0] word_check1,
//	input [6:0] word_check2,
	
	input clk,		
	input reset,
	input enable,
	input sw_done,
	input game_bt,
	input confirm_bt,
	
	//these words display on the 7-segment
	input  [6:0] word_0,
	input  [6:0] word_1,
	input  [6:0] word_2,
	
	output reg game_flag, //debug will removed
	output reg [3:0] score);


reg [6:0] word_check0;
reg [6:0] word_check1;
reg [6:0] word_check2;

reg confirm_flag;

always @(posedge clk) begin

	if(reset == 0) begin
		score = 0;
		game_flag = 0;	
		confirm_flag = 0;	
		word_check0 = 7'b0000000;
		word_check1 = 7'b0000000;
		word_check2 = 7'b0000000;		
	end
	
	else if(game_bt == 0) begin	//load checking word, won't allow to load another word if not matching previous
		game_flag = 1;
		confirm_flag = 0;
		word_check0 = 7'b0001111;
		word_check1 = 7'b0001000;
		word_check2 = 7'b1000110;
	end
	
	else begin
		if(game_flag == 1) begin
		
			if(enable == 1 && sw_done == 1 && confirm_bt == 0 && confirm_flag == 0)	//confirm and sw_done
				confirm_flag = 1;
				
			if(confirm_flag == 1)
				if(word_0 == word_check0 && word_1 == word_check1 && word_2 == word_check2 && game_flag == 1) begin	//check matching word
					confirm_flag = 0;
					score = score + 4'd1;
					game_flag = 0;
				end
		end	
				
	end
		
end



endmodule	